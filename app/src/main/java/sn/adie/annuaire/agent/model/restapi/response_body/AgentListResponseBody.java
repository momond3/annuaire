package sn.adie.annuaire.agent.model.restapi.response_body;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import sn.adie.annuaire.agent.model.entity.Agent;

/**
 * Created by rygelouv on 04/12/2016.
 */

public class AgentListResponseBody
{
    @SerializedName("meta")
    public MetaBean meta;
    @SerializedName("data")
    public List<Agent> data;

    public static class MetaBean
    {
        @SerializedName("pagination")
        public Pagination pagination;

        public static class Pagination
        {
            @SerializedName("total")
            public int total;
            @SerializedName("count")
            public int count;
            @SerializedName("per_page")
            public int perPage;
            @SerializedName("current_page")
            public int currentPage;
            @SerializedName("total_pages")
            public int totalPages;
            @SerializedName("links")
            public Links links;

            public static class Links
            {
                @SerializedName("next")
                public String next;
                @SerializedName("previous")
                public String previous;
            }
        }
    }
}
