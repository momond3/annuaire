package sn.adie.annuaire.agent.model.restapi;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import sn.adie.annuaire._commons.api.BaseAPIClient;
import sn.adie.annuaire.agent.model.restapi.response_body.AgentByMatriculeResponseBody;
import sn.adie.annuaire.agent.model.restapi.response_body.AgentListResponseBody;

/**
 * Created by rygelouv on 05/12/2016.
 *
 * This is the REST API client using Retrofit library
 * This class contains all the endpoints
 */

public interface AgentAPIClient extends BaseAPIClient
{
    @GET("/agents")
    Call<AgentListResponseBody> agents(
            @Query("include") String includeRessource,
            @Query("perpage") int itemPerPage
    );

    @GET("agents/{matricule}")
    Call<AgentByMatriculeResponseBody> agentByMatricule(
            @Path("matricule") String matricule,
            @Query("include") String includeRessource
    );

    @GET("search")
    Call<AgentListResponseBody> search(
            @Query("include") String includeRessource,
            @Query("perpage") int itemPerPage,
            @Query("query") String query
    );
}
