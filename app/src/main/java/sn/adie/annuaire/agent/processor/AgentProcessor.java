package sn.adie.annuaire.agent.processor;

import android.content.Context;

import sn.adie.annuaire._commons.api.BaseAPIHandler;
import sn.adie.annuaire._commons.database.DataBaseEntityHandler;
import sn.adie.annuaire._commons.processor.BaseProcessor;
import sn.adie.annuaire.agent.model.database.AgentDBHandler;
import sn.adie.annuaire.agent.model.restapi.AgentAPIClientHandler;

/**
 * Created by rygelouv on 05/12/2016.
 */

public class AgentProcessor extends BaseProcessor
{
    private static AgentProcessor mInstance;

    public static synchronized AgentProcessor getInstance(Context context)
    {
        if (mInstance == null)
            mInstance = new AgentProcessor(context);
        return mInstance;
    }

    public AgentProcessor(Context context)
    {
        super(new AgentAPIClientHandler(context),
                context,
                AgentDBHandler.getInstance());
    }

    public void getAgents(int itemPerpage)
    {
        //this.mDataBaseEntityHandler.getEntityListByPage()
    }
}
