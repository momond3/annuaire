package sn.adie.annuaire.agent.model.entity;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by momo on 02/12/16.
 */

public class Structure extends RealmObject
{
    @SerializedName("libelle")
    public String libelle;
    @SerializedName("sigle")
    public String sigle;
    @SerializedName("adresse")
    public String adresse;
    @SerializedName("telephone")
    public String telephone;
    @SerializedName("email")
    public String email;
}
