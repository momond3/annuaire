package sn.adie.annuaire.agent.model.restapi.response_body;

import com.google.gson.annotations.SerializedName;

import sn.adie.annuaire.agent.model.entity.Agent;

/**
 * Created by rygelouv on 05/12/2016.
 */
public class AgentByMatriculeResponseBody
{
    @SerializedName("data")
    public Agent data;
}
