package sn.adie.annuaire.agent.model.database;

import android.content.Context;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import sn.adie.annuaire._commons.database.DataBaseEntityHandler;
import sn.adie.annuaire.agent.model.entity.Agent;
import sn.adie.annuaire.utils.LogUtils;

/**
 * Created by rygelouv on 04/12/2016.
 *
 * This class provide database access to agent Entity.
 */

public class AgentDBHandler implements DataBaseEntityHandler<Agent>
{
    private static final String TAG = LogUtils.makeLogTag(AgentDBHandler.class);

    private Realm realm;
    private static AgentDBHandler mInstance;

    public static synchronized AgentDBHandler getInstance()
    {
        if (mInstance == null)
            mInstance = new AgentDBHandler();
        return mInstance;
    }

    public AgentDBHandler()
    {
        this.realm = Realm.getDefaultInstance();
    }

    @Override
    public RealmResults<Agent> getEntityList()
    {
        return realm.where(Agent.class).findAll();
    }

    @Override
    public RealmResults<Agent> getEntityListByPage(int indexFrom, int indexTo)
    {
        return (RealmResults<Agent>)realm.where(Agent.class).findAll().subList(indexFrom, indexTo);
    }

    @Override
    public Agent getEntity()
    {
        return null;
    }

    @Override
    public void deleteEntity()
    {
        // There will no deleting for agents
    }

    @Override
    public void addEntity(Agent entity)
    {

    }

    @Override
    public void addEntityList(List<Agent> entityList)
    {

    }

    @Override
    public void releaseRealm()
    {
        this.realm.close();
    }
}
