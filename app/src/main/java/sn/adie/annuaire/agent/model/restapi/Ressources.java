package sn.adie.annuaire.agent.model.restapi;

/**
 * Created by rygelouv on 05/12/2016.
 */

public class Ressources
{
    public static final String STRUCTURE_RESSOURCE = "structure";
    public static final String AGENT_RESSOURCE = "agent";
}
