package sn.adie.annuaire.agent.model.entity;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by rygelouv on 05/12/2016.
 */

public class StructureData extends RealmObject
{
    @SerializedName("data")
    public Structure data;
}
