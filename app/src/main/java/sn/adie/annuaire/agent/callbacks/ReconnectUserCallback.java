package sn.adie.annuaire.agent.callbacks;

import sn.adie.annuaire._commons.errors.ReconnectUserError;

/**
 * Created by rygelouv on 06/12/2016.
 */

public interface ReconnectUserCallback
{
    void onUserReconnected(String token);

    void onReconnectFailed(ReconnectUserError error);
}
