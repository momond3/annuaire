package sn.adie.annuaire.agent.model.restapi;

import android.content.Context;
import android.util.Log;

import com.orhanobut.logger.Logger;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sn.adie.annuaire._commons.api.BaseAPIHandler;
import sn.adie.annuaire._commons.errors.ReconnectUserError;
import sn.adie.annuaire.agent.callbacks.ReconnectUserCallback;
import sn.adie.annuaire.agent.model.restapi.response_body.AgentListResponseBody;
import sn.adie.annuaire.auth.processor.AuthProcessor;
import sn.adie.annuaire.utils.LogUtils;

/**
 * Created by rygelouv on 05/12/2016.
 */

public class AgentAPIClientHandler extends BaseAPIHandler<AgentAPIClient> implements ReconnectUserCallback
{
    private static final String TAG = LogUtils.makeLogTag(AgentAPIClientHandler.class);

    private static final String KEY_PREFIX = "AGENT_API_CLIENT_";

    private static final String agents = KEY_PREFIX + "agents";
    private static final String agentByMatricule = KEY_PREFIX + "agentByMatricule";
    private static final String search = KEY_PREFIX + "search";


    public AgentAPIClientHandler(Context context)
    {
        super(context, AgentAPIClient.class);
    }

    public long agents(final Integer itemPerPage)
    {
        Logger.e(TAG, "Trying to get agents list");

        if (pendingRequest.containsKey(agents))
            return pendingRequest.get(agents);

        final long requestId = generateRequestId();
        pendingRequest.put(agents, requestId);

        Call<AgentListResponseBody> call = mClient.agents(
                Ressources.STRUCTURE_RESSOURCE,
                itemPerPage
        );

        call.enqueue(new Callback<AgentListResponseBody>()
        {
            @Override
            public void onResponse(Call<AgentListResponseBody> call, Response<AgentListResponseBody> response)
            {
                if (response.isSuccessful())
                {
                    Logger.e("Agent list retreived with SUCCESS");

                }
                else if (response.code() == 401)
                {
                    requestStack.put(agents, itemPerPage);
                    endRequest(requestId);
                    new AuthProcessor(mContext).loginOnSessionExpired(AgentAPIClientHandler.this);
                }
                else
                {
                    Logger.e("Get Agent list Failed");
                }
                endRequest(requestId);
            }

            @Override
            public void onFailure(Call<AgentListResponseBody> call, Throwable t)
            {
                Logger.e("Get Agent list Failed");
                endRequest(requestId);
            }
        });

        return requestId;
    }

    @Override
    public void onUserReconnected(String token)
    {
        Set entries = requestStack.entrySet();

        for (Object entryIter : entries)
        {
            Map.Entry entry = (Map.Entry) entryIter;
            String requestKey = (String) entry.getKey();
            switch (requestKey)
            {
                case agents:
                    agents((Integer) entry.getValue());
                    break;
            }
        }
    }

    @Override
    public void onReconnectFailed(ReconnectUserError error)
    {

    }
}
