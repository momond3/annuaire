package sn.adie.annuaire._commons.api;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


/**
 * Created by rygelouv on 05/12/2016.
 */

public abstract class BaseAPIHandler<S extends BaseAPIClient>
{
    protected static Object lock = new Object();
    protected static Map<String, Long> pendingRequest = new HashMap<>();
    protected static Map<String, Object> requestStack = new HashMap<>();
    protected Context mContext;
    protected S mClient;

    public BaseAPIHandler(Context mContext, Class<S> sericeClass)
    {
        this.mContext = mContext;
        this.mClient = ServiceGenerator.createService(sericeClass);
    }

    protected long generateRequestId()
    {
        return UUID.randomUUID()
                .getLeastSignificantBits();
    }

    protected static boolean endRequest(long requestId)
    {
        synchronized (lock)
        {
            if (pendingRequest.containsValue(requestId))
            {
                pendingRequest.values().remove(requestId);
                return true;
            }
            return false;
        }
    }

    protected  static boolean removeRequest(String requestKey)
    {
        synchronized (lock)
        {
            if (requestStack.containsKey(requestKey))
            {
                requestStack.remove(requestKey);
                return true;
            }
            return false;
        }
    }

}
