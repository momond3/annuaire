package sn.adie.annuaire._commons.api;

/**
 * Created by rygelouv on 05/12/2016.
 */

public class Config
{
    public static final String DEBUG_BASE_URL = "http://104.236.126.136/api/v1/";
    public static final String PROD_BASE_URL = "";
    //public static final String BASE_URL = DEBUG_BASE_URL; // comment in prod
    public static final String BASE_URL = DEBUG_BASE_URL;
}
