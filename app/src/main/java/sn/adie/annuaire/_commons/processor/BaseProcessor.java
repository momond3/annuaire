package sn.adie.annuaire._commons.processor;

import android.content.Context;

import io.realm.RealmObject;
import sn.adie.annuaire._commons.api.BaseAPIHandler;
import sn.adie.annuaire._commons.database.DataBaseEntityHandler;

/**
 * Created by rygelouv on 05/12/2016.
 */

public abstract class BaseProcessor
{
    protected BaseAPIHandler mBaseAPIHandler;
    protected Context mContext;
    protected DataBaseEntityHandler mDataBaseEntityHandler;

    public BaseProcessor(BaseAPIHandler mBaseAPIHandler,
                         Context mContext,
                         DataBaseEntityHandler mDataBaseEntityHandler)
    {
        this.mBaseAPIHandler = mBaseAPIHandler;
        this.mContext = mContext;
        this.mDataBaseEntityHandler = mDataBaseEntityHandler;
    }
}
