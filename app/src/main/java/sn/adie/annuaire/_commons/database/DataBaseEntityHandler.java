package sn.adie.annuaire._commons.database;

import java.util.List;

import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by rygelouv on 04/12/2016.
 *
 * This class represents the parent of all the Database handler (accessors)
 * The methods defined here will be implemented by all the children of this class
 * Each child will manage specific Annuaire Entity.
 */

public interface DataBaseEntityHandler<T extends RealmObject>
{
    RealmResults<T> getEntityList();

    RealmResults<T> getEntityListByPage(int page, int offset);

    T getEntity();

    void deleteEntity();

    void addEntity(T entity);

    void addEntityList(List<T> entityList);

    void releaseRealm();
}
