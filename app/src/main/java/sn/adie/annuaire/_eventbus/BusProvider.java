package sn.adie.annuaire._eventbus;

import com.squareup.otto.Bus;

/**
 * Created by rygelouv on 13/08/16.
 *
 * Will provide single access to the otto BUS
 */
public class BusProvider
{
    private static final Bus BUS = new MainThreadBus();

    public synchronized static Bus getInstance()
    {
        return BUS;
    }

    private BusProvider()
    {
        // Don't allow public instantiation
    }
}