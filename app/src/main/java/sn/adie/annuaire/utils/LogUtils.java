package sn.adie.annuaire.utils;

import android.util.Log;


/**
 * Handle log messages.
 * Created by littlefinger on 15/09/14.
 *
 * Edited by rygelouv
 */
public class LogUtils
{

    private static final String TAG_PREFIX = "TAG_";
    private static final int LOG_PREFIX_LENGTH = TAG_PREFIX.length();
    private static final int MAX_LOG_TAG_LENGTH = 23;

    public static String makeLogTag(String str) {
        if (str.length() > MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH) {
            return TAG_PREFIX +
                    str.substring(0, MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH - 1).toUpperCase();
        }

        return TAG_PREFIX + str.toUpperCase();
    }

    public static String makeLogTag(Class cls) {
        return makeLogTag(cls.getSimpleName().toUpperCase());
    }


    public static void LOGI(final String tag, String message, String devCodeName) {
        Log.i(tag, devCodeName + ": " + message);
    }

    public static void LOGI(final String tag, String message, Throwable cause) {
        Log.i(tag, message, cause);
    }

    public static void LOGW(final String tag, String message, String devCodeName) {
        Log.w(tag, devCodeName + ": " + message);
    }

    public static void LOGW(final String tag, String message, Throwable cause) {
        Log.w(tag, message, cause);
    }

    public static void LOGE(final String tag, String message, String devCodeName) {
        Log.e(tag, devCodeName + ": " + message);
    }

    public static void LOGE(final String tag, String message, Throwable cause) {
        Log.e(tag, message, cause);
    }

    private LogUtils() {

    }
}
