package sn.adie.annuaire.account.model.entity;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import sn.adie.annuaire.agent.model.entity.StructureData;

/**
 * Created by rygelouv on 04/12/2016.
 */

public class User extends RealmObject
{
    @SerializedName("matricule")
    public String matricule;
    @SerializedName("prenom")
    public String prenom;
    @SerializedName("nom")
    public String nom;
    @SerializedName("telephone_cdma")
    public String telephoneCdma;
    @SerializedName("telephone_bureau")
    public String telephoneBureau;
    @SerializedName("fonction")
    public String fonction;
    @SerializedName("adresse")
    public String adresse;
    @SerializedName("email")
    public String email;
    @SerializedName("photo_url")
    public String photoUrl;
    @SerializedName("biographie")
    public String biographie;
    @SerializedName("structure")
    public StructureData structure;
}
