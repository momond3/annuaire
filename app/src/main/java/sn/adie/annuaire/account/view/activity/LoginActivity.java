package sn.adie.annuaire.account.view.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import sn.adie.annuaire.R;
import sn.adie.annuaire._eventbus.BusProvider;
import sn.adie.annuaire.auth.events.APIFailureResponse;
import sn.adie.annuaire.auth.events.APISuccessResponse;
import sn.adie.annuaire.auth.processor.AuthProcessor;

public class LoginActivity extends AppCompatActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.login_input)
    EditText loginInput;
    @BindView(R.id.password_input)
    EditText passwordInput;
    @BindView(R.id.connexion_btn)
    Button connexionBtn;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        BusProvider.getInstance().register(this);
        setSupportActionBar(toolbar);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Veuillez patienter");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);

    }

    @OnClick(R.id.connexion_btn)
    public void submitInformation()
    {
        String txtLogin = loginInput.getText().toString();
        String txtPassword = passwordInput.getText().toString();

        if (txtLogin.trim().isEmpty() | txtPassword.trim().isEmpty())
        {
            if (txtLogin.trim().isEmpty())
                loginInput.setError("Veuillez entrer le login");

            if (txtPassword.trim().isEmpty())
                passwordInput.setError("Veuillez entrer le mot de passe");
        }
        else
        {
            progressDialog.show();
            new AuthProcessor(this).login(txtLogin,txtPassword);
        }

    }

    @Subscribe
    public void onLoginSucces(APISuccessResponse successResponse)
    {
        progressDialog.dismiss();
        Toast.makeText(this, "Conexion Réusssie", Toast.LENGTH_SHORT).show();
    }

    public void onLoginFailed(APIFailureResponse failureResponse)
    {
        progressDialog.dismiss();
        Toast.makeText(this, "Erreur de Connexion", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        BusProvider.getInstance().unregister(this);
        super.onDestroy();
    }
}
