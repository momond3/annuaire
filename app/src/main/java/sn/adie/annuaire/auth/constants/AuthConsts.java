package sn.adie.annuaire.auth.constants;

/**
 * Created by rygelouv on 04/12/2016.
 *
 * This class contains all the constants relative to the authentication package
 */

public class AuthConsts
{
    public static final String AUTH_TYPE_USER_LOGIN = "USER_LOGIN";
    public static final String AUTH_TYPE_SYSTEM_LOGIN = "SYSTEM_LOGIN";
}
