package sn.adie.annuaire.auth.model.session;

import sn.adie.annuaire.account.model.entity.User;

/**
 * Created by rygelouv on 04/12/2016.
 *
 * This class aims to manage everything about the user's session
 * The current user is provided by this class and all the operations related to session management.
 * The session uses a shared preferences system
 */

public class SessionManager
{
    public static String getUsername()
    {
        // TODO implement the logic code for getting the username from the shared prefs
        return "";
    }

    public static String getPassword()
    {
        // TODO implement the logic code for getting the password from the shared prefs
        return "";
    }

    public static void storeLoginCredentials(String username, String password)
    {
        // TODO Implement the code for storing the credentials in the shared prefs
    }

    public static void creatSession(User user, String token)
    {

    }

    public static String getSessionToken()
    {
        // TODO implement the logic code for getting the Token from the shared prefs
        return "";
    }

    public static void setSessionToken(String token)
    {
        // TODO Implement the code for storing the token in the shared prefs
    }
}
