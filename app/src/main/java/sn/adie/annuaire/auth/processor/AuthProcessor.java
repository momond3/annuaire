package sn.adie.annuaire.auth.processor;

import android.content.Context;

import com.squareup.otto.Subscribe;

import sn.adie.annuaire._commons.processor.BaseProcessor;
import sn.adie.annuaire._eventbus.BusProvider;
import sn.adie.annuaire.agent.callbacks.ReconnectUserCallback;
import sn.adie.annuaire.auth.constants.AuthConsts;
import sn.adie.annuaire.auth.events.APISuccessResponse;
import sn.adie.annuaire.auth.model.restapi.AuthAPIClientHandler;
import sn.adie.annuaire.auth.model.restapi.request_body.LoginRequestBody;
import sn.adie.annuaire.auth.model.session.SessionManager;

/**
 * Created by rygelouv on 06/12/2016.
 */

public class AuthProcessor extends BaseProcessor
{
    private String mAuthenticationType;
    private ReconnectUserCallback mCallback;

    public AuthProcessor(Context context)
    {
        super(new AuthAPIClientHandler(context),
                context,
                null
                );
        BusProvider.getInstance().register(this);
    }

    public void login(String username, String password)
    {
        mAuthenticationType = AuthConsts.AUTH_TYPE_USER_LOGIN;
        ((AuthAPIClientHandler)this.mBaseAPIHandler)
                .login(new LoginRequestBody(username, password));
    }

    public void loginOnSessionExpired(ReconnectUserCallback callback)
    {
        this.mAuthenticationType = AuthConsts.AUTH_TYPE_SYSTEM_LOGIN;
        this.mCallback = callback;

        ((AuthAPIClientHandler)this.mBaseAPIHandler)
                .login(new LoginRequestBody(
                        SessionManager.getUsername(),
                        SessionManager.getPassword()
                ));
    }

    @Subscribe
    public void onLoginSucessEvent(APISuccessResponse.LoginSuccessEvent event)
    {
        switch (mAuthenticationType)
        {
            case AuthConsts.AUTH_TYPE_USER_LOGIN:
                SessionManager.creatSession(event.getResponseBody().user, event.getResponseBody().token);
                break;
            case AuthConsts.AUTH_TYPE_SYSTEM_LOGIN:
                SessionManager.setSessionToken(event.getResponseBody().token);
                mCallback.onUserReconnected(event.getResponseBody().token);
                break;
        }
    }

    public void relase()
    {
        BusProvider.getInstance().unregister(this);
    }

}
