package sn.adie.annuaire.auth.model.restapi.response_body;

import com.google.gson.annotations.SerializedName;

import sn.adie.annuaire.account.model.entity.User;

/**
 * Created by rygelouv on 05/12/2016.
 */

public class LoginResponseBody
{
    @SerializedName("data")
    public User user;
    @SerializedName("token")
    public String token;
}
