package sn.adie.annuaire.auth.events;

import sn.adie.annuaire.auth.model.restapi.response_body.LoginResponseBody;

/**
 * Created by rygelouv on 04/12/2016.
 */

public class APISuccessResponse
{
    public static class LoginSuccessEvent
    {
        private LoginResponseBody responseBody;

        public LoginSuccessEvent(LoginResponseBody responseBody)
        {
            this.responseBody = responseBody;
        }

        public LoginResponseBody getResponseBody()
        {
            return responseBody;
        }

        public void setResponseBody(LoginResponseBody responseBody)
        {
            this.responseBody = responseBody;
        }
    }
}
