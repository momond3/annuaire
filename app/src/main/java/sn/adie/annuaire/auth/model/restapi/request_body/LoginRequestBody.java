package sn.adie.annuaire.auth.model.restapi.request_body;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rygelouv on 05/12/2016.
 */

public class LoginRequestBody
{

    @SerializedName("username")
    public String username;
    @SerializedName("password")
    public String password;

    public LoginRequestBody(String username, String password)
    {
        this.username = username;
        this.password = password;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }
}
