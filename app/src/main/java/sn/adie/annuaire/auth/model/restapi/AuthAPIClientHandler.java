package sn.adie.annuaire.auth.model.restapi;

import android.content.Context;

import com.orhanobut.logger.Logger;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sn.adie.annuaire._commons.api.BaseAPIHandler;
import sn.adie.annuaire._eventbus.BusProvider;
import sn.adie.annuaire.auth.events.APIFailureResponse;
import sn.adie.annuaire.auth.events.APISuccessResponse;
import sn.adie.annuaire.auth.model.restapi.request_body.LoginRequestBody;
import sn.adie.annuaire.auth.model.restapi.response_body.LoginResponseBody;
import sn.adie.annuaire.utils.LogUtils;

/**
 * Created by rygelouv on 05/12/2016.
 */

public class AuthAPIClientHandler extends BaseAPIHandler<AuthAPIClient>
{
    private static final String TAG = LogUtils.makeLogTag(AuthAPIClientHandler.class);
    private static final String KEY_PREFIX = "AUTH_API_CLIENT_";

    private static final String login = KEY_PREFIX + "login";


    public AuthAPIClientHandler(Context context)
    {
        super(context, AuthAPIClient.class);
    }

    public long login(final LoginRequestBody requestBody)
    {
        Logger.e(TAG, "Trying to get agents list");

        if (pendingRequest.containsKey(login))
            return pendingRequest.get(login);

        final long requestId = generateRequestId();
        pendingRequest.put(login, requestId);

        //Call<LoginResponseBody> call =((AuthAPIClient)mClient).login(requestBody);
        Call<LoginResponseBody> call = mClient.login(requestBody);
        call.enqueue(new Callback<LoginResponseBody>()
        {
            @Override
            public void onResponse(Call<LoginResponseBody> call,
                                   Response<LoginResponseBody> response)
            {
                if (response.isSuccessful())
                {
                    Logger.e(TAG,"Login Succes");
                    BusProvider.getInstance().post(new
                            APISuccessResponse.LoginSuccessEvent(response.body())
                    );
                }
                else
                {
                    Logger.e(TAG,"Login Failed code"+response.code());
                    BusProvider.getInstance().post(new APIFailureResponse.LoginFailedEvent(response.code()));
                }
                endRequest(requestId);
            }
            @Override
            public void onFailure(Call<LoginResponseBody> call, Throwable t)
            {
                BusProvider.getInstance().post(new APIFailureResponse.LoginFailedEvent());
                endRequest(requestId);
            }
        });

        return requestId;
    }
}
