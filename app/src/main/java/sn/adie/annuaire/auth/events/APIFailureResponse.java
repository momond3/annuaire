package sn.adie.annuaire.auth.events;

/**
 * Created by rygelouv on 04/12/2016.
 */

public class APIFailureResponse
{

    public static class LoginFailedEvent
    {
        private int code;

        public LoginFailedEvent() {
        }

        public LoginFailedEvent(int code) {
            this.code = code;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }
    }
}
