package sn.adie.annuaire.auth.model.restapi;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import sn.adie.annuaire._commons.api.BaseAPIClient;
import sn.adie.annuaire.auth.model.restapi.request_body.LoginRequestBody;
import sn.adie.annuaire.auth.model.restapi.response_body.LoginResponseBody;

/**
 * Created by rygelouv on 05/12/2016.
 */

public interface AuthAPIClient extends BaseAPIClient
{
    @POST("login")
    Call<LoginResponseBody> login(
            @Body LoginRequestBody requestBody
    );

    @GET("token/valid")
    Call<Boolean> validateToken(

    );
}
